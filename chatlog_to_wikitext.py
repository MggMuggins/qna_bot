#!/bin/env python

import sys

from tinydb import Query, TinyDB

USAGE = """Usage:

chatlog_to_wikitext.py SOURCE OUTPUT"""

if len(sys.argv) <= 2:
    print(USAGE)
    exit(1)

db = TinyDB(sys.argv[1])
outfile = open(sys.argv[2], "a")

for msg in db:
    outfile.write("'''{}:'''\n{}\n\n".format(msg["usr"], msg["msg"]))
