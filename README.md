# Q&A Bot
This is the python script which backs my "Ned" bot for the LOTR Mod Discord Q and A sessions. This repo also includes a few tools for processing the json databases that the bot generates.

If you want to call the json db output an API, it's by no means stable. It will likely change commit to commit in this repo. If you want to use it though, qna_bot saves to the dbs using [TinyDB](https://github.com/msiemens/tinydb/). Use it to interface with them. If at some point the format becomes more or less stable, I'll document it.

# Configuration
The bot uses a toml file for configuration. 

```toml
# Bot's token from the developer dashboard, make sure it's the bot's and not the application's
token = "<token here>"

# Name of the channel in which questions are asked and answered
question_channel = "q-and-a"

# List of users who are allowed to answer questions and use ..lock/..unlock
# These are the long numerical id's, not the username or short number following
#   Mine is provided as an example
op_users = [
    405147332922048512, # Samwise
    # ...
]

# Database filenames to write to
#   The bot will append to existing dbs, and the dbs persist over restarts
chatlog = "chatlog.json"
answers = "answers.json"
```

# Usage
## Commands
qna_bot includes two commands, `..lock` and `..unlock`. The respectively allow and disallow the "Send Messages" permission for "@everyone" in the `question_channel`. **They only work if the commands are sent in the question channel.**

## Logging
qna_bot logs all messages sent to `question_channel` in the `chatlog` json file.

## Answers
qna_bot primarily listens for messages that are sent by the users found in the `op_users` list in the `question_channel`. If a message that meets those criteria mentions another user, then the content of last message sent by the mentioned user (or users) and the content of the mentioning message are written to the `answers` json db.
