#!/bin/env python

import re
import sys

from tinydb import Query, TinyDB

USAGE = """Usage:

answers_to_wikitext.py SOURCE OUTPUT"""

if len(sys.argv) <= 2:
    print(USAGE)
    exit(1)

db = TinyDB(sys.argv[1])
output = open(sys.argv[2], "a")

for q in db:
    without_mention = re.sub("<@!{0,1}\d{18}> ", "", q["answer"])
    output.write("'''Q: '''{}\n\n'''A: '''{}\n<hr/>\n".format(q["questions"][0], without_mention))
