#!/bin/env python

import logging
from logging import info
from sys import argv
import traceback

from discord import Client
from tinydb import Query, TinyDB
import toml

async def set_everyone_perms(message, should_send):
    for role in message.guild.roles:
        if role.name == "@everyone":
            await message.channel.set_permissions(role, send_messages=should_send)
            break

class QABot(Client):
    
    def __init__(self, config_path):
        config = open(config_path)
        config = toml.loads(config.read())
        
        self.chatlog = TinyDB(config["chatlog"])
        self.answers = TinyDB(config["answers"])
        
        self.channel_name = config["question_channel"]
        self.allowed_users = config["op_users"] # List of int IDs
        self.token = config["token"]
        
        super().__init__()
    
    async def on_ready(self):
        print("Successful logon as {}".format(self.user))
    
    async def on_message(self, message):
        if message.channel.name == self.channel_name:
            
            self.chatlog.insert({"usr": message.author.name, "msg": message.content})
            
            if message.author.id in self.allowed_users:
                if message.content.startswith("..lock"):
                    await set_everyone_perms(message, False)
                elif message.content.startswith("..unlock"):
                    await set_everyone_perms(message, True)
                
                if len(message.mentions) > 0:
                    if await self.process_question(message):
                        # NOTE: This _is_ and emoji, not a space
                        await message.add_reaction("🔏")
                    else:
                        await message.add_reaction("🩸")
    
    async def process_question(self, message):
        try:
            entry = {"questions": [], "answer": message.content}
            
            handled_mentions = set()
            async for msg in message.channel.history(limit=150):
                if msg.author in message.mentions and msg.author.id not in handled_mentions:
                    entry["questions"].append(msg.content)
                    handled_mentions.add(msg.author.id)
            
            self.answers.insert(entry)
        except Exception:
            traceback.print_exc()
            return False
        else:
            return True

if __name__ == "__main__":
    bot = QABot("config.toml")
    bot.run(bot.token)
